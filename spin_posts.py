from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
import os
import random
import gspread
import time
import datetime
import asana
from itertools import zip_longest
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

# chrome_options = Options()
# chrome_options.add_argument("--headless")

ASANA_CLIENT_KEY = os.getenv('ASANA_CLIENT_KEY')
client = asana.Client.access_token(ASANA_CLIENT_KEY)

gc = gspread.service_account(
    filename='google_credentials.json')
sh = gc.open('Serp Site List')
new_domain_ss = sh.worksheet('Selenium Builds')


new_domain_list = new_domain_ss.col_values(1)
pw_list = new_domain_ss.col_values(2)
date_spun = new_domain_ss.col_values(3)
domain_pw_list = list(zip_longest(
    new_domain_list, pw_list, date_spun, fillvalue='-'))

# driver = webdriver.Chrome(chrome_options=chrome_options)
domain_data = new_domain_ss.get_all_records()

# Begin loop here
for idx, i in enumerate(domain_data):
    
    driver = webdriver.Chrome()
    time_arr = []
    url = i['URL']
    user = i['User']
    password = i['Password']
    shortcode = i['Shortcode']
    starting_post = int(i['Starting Post'] or 0)
    total_posts = int(i['Total Posts'] or 0)
    date_last_spun = i['Date Last Spun']
    number_to_spin = int(i['Number To Spin'] or 0)

    driver.get(url)

    try:
        date = datetime.datetime.strptime(date_last_spun, '%m/%d/%Y').date()
        if date >= datetime.date.today():
            print(f'{url} Already Spun Today')
            # continue
    except:
        print('no date in cell')
        pass

    time.sleep(3)
    try:
        login = driver.find_element_by_id('user_login')
        pw = driver.find_element_by_id('user_pass')
        button = driver.find_element_by_id('wp-submit')

        login.clear()
        login.send_keys(user)

        pw.clear()
        pw.send_keys(str(password))
        button.click()
    except:
        print(f'Unable to login to {url}')
        # continue
    time.sleep(3)

    shortcode_url = f"{url}&page=serpshaker-shortcodes"
    driver.get(shortcode_url)
    shortcode_table = driver.find_elements_by_tag_name('td')
    if not total_posts or total_posts == 0:
        for idx, i in enumerate(shortcode_table):
            print(i.text)
            if i.text in shortcode.replace('{', '').replace('}', ''):
                print('Shortcode found')
                print(f"shortcode number is {shortcode_table[idx + 1].text}")
                total_posts = int(shortcode_table[idx + 1].text.split(' ')[0])
                # Need to update the sheet with the total posts number at some point
                # domain_data.update_cell(count+1, 3, str(datetime.datetime.now().date()))
                break

    remaining_posts = True
    if starting_post > total_posts:
        remaining_posts = False
    count = 0
    total_posts_built = 0
    while remaining_posts:
        time.sleep(30)
        start = time.time()
        if count < 1:
            driver.get(url)
            try:
                element = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "row-title"))
                )
            except:
                try:
                    result = client.tasks.create_task({'assignee': '136251961484573', 'due_on': str(datetime.datetime.now(
                    )), 'name': f'Check {url} for ability for script to login', 'projects': '1179067911189055'})
                except:
                    print(f'Problem Logging In To {url}')
                    remaining_posts = False
            table = driver.find_elements_by_class_name('row-title')
            for i in table:
                print(i.get_attribute('aria-label'))
                post_name = i.get_attribute('aria-label')
                if shortcode in post_name:
                    post_url = i.get_attribute('href')
                    driver.get(post_url)
                else:
                    print('No shortcode found')
                    # continue
            count += 1
            time.sleep(3)
        try:
            num_posts_element = driver.find_element_by_name(
                'serpshaker[numberOfPosts]')
            number_of_posts_value = int(
                num_posts_element.get_attribute('value') or 0)

            num_posts_to_skip_element = driver.find_element_by_name(
                'serpshaker[numberOfSkip]')
            num_posts_to_skip_value = int(
                num_posts_to_skip_element.get_attribute('value') or 0)

            if num_posts_element and number_of_posts_value < 10:
                # This should run on the first set of spun posts
                num_posts_element.clear()
                num_posts_element.send_keys(starting_post + number_to_spin)
                num_posts_to_skip_element.clear()
                num_posts_to_skip_element.send_keys(starting_post)

                numberOfDays = driver.find_element_by_name(
                    'serpshaker[numberOfDays]')
                numberOfDays.clear()
                num_days_to_set = random.randint(365, 3650)
                numberOfDays.send_keys(str(num_days_to_set))

                backdate = driver.find_element_by_css_selector(
                    "input[type='radio'][value='past']")
                backdate.click()

                ss_category = driver.find_element_by_name('ss_category')
                ss_category.clear()
                ss_category.send_keys(shortcode.replace('{', '').replace('}', ''))
                try:
                    build_button = driver.find_element_by_id('build')
                    build_button.click()
                except:
                    build_button = False
                    print('No Build Button Found')
                if not build_button:
                    try:
                        delete_posts = driver.find_element_by_id('unbuild')
                        delete_posts.click()
                        build_button = driver.find_element_by_id('build')
                        build_button.click()
                    except:
                        print('didnt work')
                    remaining_posts = False
                    continue

                starting_post = starting_post + number_to_spin
                total_posts_built += number_to_spin

            else:
                # try:
                #     element = WebDriverWait(driver, 10).until(
                #         EC.presence_of_element_located(
                #             (By.CLASS_NAME, "row-title"))
                #     )
                # except:
                #     try:
                #         result = client.tasks.create_task({'assignee': '136251961484573', 'due_on': str(datetime.datetime.now(
                #         )), 'name': f'Check {url} for ability for script to login', 'projects': '1179067911189055'})
                #     except:
                #         print(f'Problem Logging In To {url}')
                #         remaining_posts = False
                num_posts_element.clear()
                num_posts_element.send_keys(starting_post + number_to_spin)
                num_posts_to_skip_element.clear()
                num_posts_to_skip_element.send_keys(starting_post)
                save_draft_button = driver.find_element_by_id('save-post')
                save_draft_button.click()
                # time.sleep(30)
                try:
                    
                    build_button = driver.find_element_by_id('build')
                    build_button.click()
                    starting_post = starting_post + number_to_spin
                    total_posts_built += number_to_spin
                except:
                    build_button = False
                    print('No Build Button Found')
                    remaining_posts = False
                    # delete_posts = driver.find_element_by_id('unbuild')
                    # delete_posts.click()

            end = time.time()
            elapsed = end - start
            time_arr.append(elapsed)
            if starting_post > total_posts:
                remaining_posts = False
        except Exception as e:
            print(e)
            remaining_posts = False
            end = time.time()
            elapsed = end - start
            time_arr.append(elapsed)
            continue

    cells_to_update = f"A{idx + 2}"
    data = [url, user, password, shortcode, starting_post, number_to_spin, total_posts, str(datetime.datetime.now().date()) ]
    new_domain_ss.update(cells_to_update, [data])

    def average(lst): 
        if lst:
            return sum(lst) / len(lst) 
        return 0

    print(f"Average time per page is {round(average(time_arr))}")
    print(f"total time   is {sum(time_arr)}")
    print(f"total pages   is {len(time_arr)}")
    print(f"total posts   is {starting_post}")
    sleepy_time = 120
    print(f"sleeping for {sleepy_time} seconds")
    time.sleep(sleepy_time)
    driver.quit()


# for item in domain_pw_list:
#     count += 1
#     domain = item[0]
#     password = item[1]
#     print(f'Spinning {domain}')
#     try:
#         date = datetime.datetime.strptime(item[2], '%m/%d/%Y').date()
#         if date >= datetime.date.today():
#             print(f'{domain} Already Spun Today')
#             continue
#     except:
#         print('no date in cell')
#         pass
#     # Find out how the domain comes in from google sheets
#     driver.get(domain)

#     time.sleep(3)
#     try:
#         login = driver.find_element_by_id('user_login')
#         pw = driver.find_element_by_id('user_pass')
#         button = driver.find_element_by_id('wp-submit')

#         login.clear()
#         login.send_keys('adam@sitegrinder.net')

#         pw.clear()
#         pw.send_keys(str(password))
#         button.click()
#     except:
#         print(f'Unable to login to {domain}')
#         continue

#     while remaining_posts:
#         driver.get(domain)
#         try:
#             element = WebDriverWait(driver, 10).until(
#                 EC.presence_of_element_located((By.CLASS_NAME, "row-title"))
#             )
#         except:

#             try:
#                 result = client.tasks.create_task({'assignee': '136251961484573', 'due_on': str(datetime.datetime.now(
#                 )), 'name': f'Check {domain} for ability for script to login', 'projects': '1179067911189055'})
#             except:
#                 print(f'Problem Logging In To {domain}')

#             continue

#         table = driver.find_elements_by_class_name('row-title')
#         last_row = table[-1].get_attribute('href')
#         driver.get(last_row)

#         time.sleep(3)

#         numberOfPosts = driver.find_element_by_name(
#             'serpshaker[numberOfPosts]')
#         number_of_posts_value = numberOfPosts.get_attribute('value')
#         # TODO Builds stop if it finds a value and the build button is found. Refactor to account for this.
#         # TODO Example - number_of_posts_value != '' and build_button == True.
#         try:
#             build_button = driver.find_element_by_id('build')
#         except:
#             if number_of_posts_value != '':
#                 delete_posts = driver.find_element_by_id('unbuild')
#                 delete_posts.click()
#             else:
#                 print('Posts already built')
#                 try:
#                     result = client.tasks.create_task({'assignee': '136251961484573', 'due_on': str(
#                         datetime.datetime.now()), 'name': f'Check {domain} for all posts spun', 'projects': '1179067911189055'})
#                 except:
#                     print(f'No FU task created for {domain}')
#                 remaining_posts = False
#                 continue
#         numberOfPosts = driver.find_element_by_name(
#             'serpshaker[numberOfPosts]')
#         numberOfPosts.clear()

#         numberOfDays = driver.find_element_by_name('serpshaker[numberOfDays]')
#         numberOfDays.clear()
#         num_days_to_set = random.randint(365, 1200)
#         numberOfDays.send_keys(str(num_days_to_set))

#         backdate = driver.find_element_by_css_selector(
#             "input[type='radio'][value='past']")
#         backdate.click()

#         post_name = driver.find_element_by_name('post_title')
#         a = post_name.get_attribute('value')
#         result = re.findall(r"\{([^|]*)\}", a)

#         ss_category = driver.find_element_by_name('ss_category')
#         ss_category.clear()
#         ss_category.send_keys(result[0])
#         build_button = driver.find_element_by_id('build')
#         build_button.click()
#     new_domain_ss.update_cell(count, 3, str(datetime.datetime.now().date()))

# driver.quit()
# # TODO Add numposts to database to stop spinning finished sites
# # TODO Turn Into Cron Job And Send Email When Spin Is Complete For Day


# # * * * * * /Users/adam.windsor/sitegrinder/src/sg-api/env/bin/python3 /Users/adam.windsor/sitegrinder/src/sg-api/site_index_check.py
