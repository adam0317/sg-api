from selenium import webdriver
from selenium.webdriver.support.ui import Select
import gspread
import time
import string
from random import *
import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

RUNCLOUD_USER = os.getenv('RUNCLOUD_USER')
PASSWORD = os.getenv('PASSWORD')

# Shaker Template - Linode 3
clone_from_server_url = 'https://manage.runcloud.io/servers/133049/webapplications/541459/tools/clone'


gc = gspread.service_account(
    filename='google_credentials.json')
sh = gc.open('Serp Site List')
new_domain_ss = sh.worksheet('Build Sheet')

driver = webdriver.Chrome()
driver.get(clone_from_server_url)

email_input = driver.find_element_by_name('email')
pw_input = driver.find_element_by_name('password')
email_input.clear()
email_input.send_keys(RUNCLOUD_USER)

pw_input.clear()
pw_input.send_keys(PASSWORD)

submit = driver.find_element_by_class_name('btn-success')
submit.click()
time.sleep(3)

domains = new_domain_ss.get_all_records()

for i in domains:
  domain = i['Domain']
  id = i['Id']
  server_to_clone_to = i['Server To Clone To']
  db_name = i['DB Name']
  if not db_name:
    db_name = 'shakertemplate'
  print(f'DB Name ======= {db_name}')

  stripped_domain = domain.replace('-', '').replace('.', '')[:22]
  driver.get(clone_from_server_url)

  time.sleep(2)

  clone_to = Select(driver.find_element_by_name('cloneTo'))
  clone_to.select_by_visible_text(server_to_clone_to)

  clone_db = driver.find_element_by_id('cloneExistingDatabase')
  clone_db.click()

  time.sleep(2)

  select_db = Select(driver.find_element_by_name('database'))

  select_db.select_by_visible_text(db_name)
  time.sleep(2)
  new_db_input = driver.find_element_by_name('newDatabase')
  new_db_input.clear()
  new_db_input.send_keys(stripped_domain)
  time.sleep(2)

  new_db_name = driver.find_element_by_name('name')
  new_db_name.clear()
  new_db_name.send_keys(stripped_domain)

  time.sleep(2)
  user = Select(driver.find_element_by_name('user'))
  user.select_by_visible_text('runcloud')
  time.sleep(2)

  domain_name_input = driver.find_element_by_name('domainName')
  domain_name_input.clear()
  domain_name_input.send_keys(domain)
  time.sleep(2)

  clone_btn = driver.find_element_by_class_name('btn-block')
  clone_btn.click()
  print(f"Cloning {domain}")
  time.sleep(60)
driver.close()
