import argparse
import asana
import datetime
import gspread
import CloudFlare
import re
import requests
import webbrowser
from time import sleep
import sys
import os
import pprint
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

parser = argparse.ArgumentParser()
parser.add_argument('--step', nargs='?', const=1, default='all')

args = parser.parse_args()


ASANA_CLIENT_KEY = os.getenv('ASANA_CLIENT_KEY')
CF_API_TOKEN = os.getenv(('CF_API_TOKEN'))
DIGITAL_OCEAN_SERVER_IP = os.getenv(('DIGITAL_OCEAN_SERVER_IP'))
EMAIL = os.getenv('EMAIL')


cf = CloudFlare.CloudFlare(email=EMAIL, token=CF_API_TOKEN)

gc = gspread.service_account(
    filename='google_credentials.json')
sh = gc.open('Serp Site List')
new_domain_ss = sh.worksheet('New Domains For Setup')
master_list = sh.worksheet('Master List')
new_domain_list = new_domain_ss.col_values(1)


def add_to_asana():
    task_gid = '1179941058250758'
    client = asana.Client.access_token(ASANA_CLIENT_KEY)
    for domain in new_domain_list:
        result = client.tasks.duplicate_task(task_gid, {
            "include": [
                "assignee",
                "subtasks",
                "projects"
            ],
            "name": domain,
        }, opt_pretty=False)
    return



create_follow_up_task('adam.com')

def add_domain_to_cloudflare():
    for zone_name in new_domain_list:
        if '.' in zone_name:
            print('Create zone %s ...' % (zone_name))
            try:
                zone_info = cf.zones.post(
                    data={'jump_start': False, 'name': zone_name})
            except CloudFlare.exceptions.CloudFlareAPIError as e:
                exit('/zones.post %s - %d %s' % (zone_name, e, e))
            except Exception as e:
                exit('/zones.post %s - %s' % (zone_name, e))
            zone_id = zone_info['id']
            try:
                r = cf.zones.settings.always_use_https.patch(
                    zone_id, data={'value': 'on'})
            except:
                print('Force SSL not set')
            print('Finished zone %s ...' % (zone_name))
        else:
            continue
    return

# UA-174583705-1


def add_dns_records_to_cloudflare():
    new_domain_dns_records = new_domain_ss.get_all_records()
    for domain in new_domain_dns_records:
        dns_records = []
        zone_name = domain['Domain']
        zones = cf.zones.get(params={'name': zone_name, 'per_page': 1})
        zone_id = zones[0]['id']

        for key, value in domain.items():
            if key == 'Domain':
                continue
            if not value:
                continue
            if key == 'Google Verification Code':
                dns_records.append(
                    {'name': '@', 'type': 'TXT', 'content': value})
            if key == 'Other Verification Code':
                dns_records.append(
                    {'name': '@', 'type': 'TXT', 'content': value})
            if key == 'Server IP':
                dns_records.append(
                    {'name': '@', 'type': 'A', 'content': value, 'proxied': True})
                dns_records.append(
                    {'name': 'www', 'type': 'CNAME', 'content': zone_name, 'proxied': True})
        print(dns_records)
        print('Create DNS records ...')
        for dns_record in dns_records:
            try:
                r = cf.zones.dns_records.post(zone_id, data=dns_record)
            except Exception as e:
                print(e)
                print(zone_id)
                print(dns_record)
    return


def get_service(api_name, api_version, scopes, key_file_location):
    """Get a service that communicates to a Google API.

    Args:
        api_name: The name of the api to connect to.
        api_version: The api version to connect to.
        scopes: A list auth scopes to authorize for the application.
        key_file_location: The path to a valid service account JSON key file.

    Returns:
        A service that is connected to the specified API.
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service


def add_to_google_analytics(domain):
    scope = 'https://www.googleapis.com/auth/analytics.edit'
    key_file_location = 'analytics-user.json'
    service = get_service(
        api_name='analytics',
        api_version='v3',
        scopes=[scope],
        key_file_location=key_file_location)
    # website_url = f"https://{domain}"
    account = os.getenv(('ANALYTICS_ACCOUNT_ID'))
    data = {
        "kind": "analytics#webproperty",
        "accountId": account,
        "name": domain,
        "websiteUrl": website_url
    }
    new_site = service.management().webproperties().insert(
        accountId=account, body=data).execute()
    print(new_site)
    # TODO Add to google sheets for part of the Selenium Build
    # TODO Add Analytics Code To Insert Headers/Footers Script In WP
    return

# TODO Move zone_name name off of New Domains For Setup Tab And Onto Master List
# TODO Add Google Search Console API and update DNS records
# TODO Find task by name in asana and change due date after indexing has occured


if args.step == 'all':
    print('Running all functions')
    add_to_asana()
    add_domain_to_cloudflare()
    add_dns_records_to_cloudflare()
if args.step == 'asana':
    print('Running Asana')
    add_to_asana()
if args.step == 'domain':
    print('Running Domain')
    add_domain_to_cloudflare()
if args.step == 'dns':
    print('Running dns')
    add_dns_records_to_cloudflare()
else:
    quit()


# {'id': 'UA-173934911-4', 'kind': 'analytics#webproperty'
#     , 'selfLink': 'https://www.googleapis.com/analytics/v3/management/accounts/173934911/webproperties/UA-173934911-4'
#     , 'accountId': '173934911', 'internalWebPropertyId': '242215117'
#     , 'name': 'asheboronet.com', 'websiteUrl': 'https://asheboronet.com'
#     , 'level': 'STANDARD', 'profileCount': 0, 'dataRetentionTtl': 'MONTHS_26'
#     , 'dataRetentionResetOnNewActivity': True, 'permissions': {
#     'effective': ['COLLABORATE', 'EDIT', 'MANAGE_USERS', 'READ_AND_ANALYZE']}
#     , 'created': '2020-08-06T02:20:10.357Z', 'updated': '2020-08-06T02:20:10.357Z'
#     , 'parentLink': {'type': 'analytics#account'
#     , 'href': 'https://www.googleapis.com/analytics/v3/management/accounts/173934911'}
#     , 'childLink': {'type': 'analytics#profiles'
#                     , 'href': 'https://www.googleapis.com/analytics/v3/management/accounts/173934911/webproperties/UA-173934911-4/profiles'}}
