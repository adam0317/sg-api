from selenium import webdriver
from google.cloud import firestore
import gspread
import time
import string
from random import *
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

gc = gspread.service_account(
    filename='google_credentials.json')
sh = gc.open('Serp Site List')
new_domain_ss = sh.worksheet('WP Final Setup')

db = firestore.Client()
pw_list = sh.worksheet('Users')

new_domain_list = new_domain_ss.get_all_records()
user = 'sg123'
password = 'wbG6Y0pVUFiE'

option = webdriver.ChromeOptions()
# option.add_argument('--no-sandbox')
# option.add_argument('--headless')
driver = webdriver.Chrome(options=option)

for idx, i in reversed(list(enumerate(new_domain_list))):
    tmp_arr = []
    domain = i['URL']
    site_title = i['Site Title']
    shaker_page_id = i['Shaker Page Id']
    offer_link = f"{i['Offer Link']}/{domain.split('.')[0]}"
    spin_the_posts = i['Spin Posts']
    category = i['Main Keyword']
    try:    
        tmp_arr.append(domain)
        tmp_arr.append(user)
        permalink_url = f"https://{i['URL']}/wp-admin/options-permalink.php"

        driver.get(permalink_url)

        if 'shakertemplate' in driver.current_url:
            print(f'*************** DB clone error {domain}')
            continue
        try:
            login = driver.find_element_by_id('user_login')
            pw = driver.find_element_by_id('user_pass')
            button = driver.find_element_by_id('wp-submit')

            login.clear()
            login.send_keys(user)

            pw.clear()
            pw.send_keys(str(password))
            button.click()
        except:
            print(f'Unable to login to {domain}')

        # Save Permalinks Twice
        time.sleep(5)
        post_name_input = driver.find_element_by_css_selector(
                              "input[type='radio'][value='/%postname%/']")
        post_name_input.click()
        
        permalink_save = driver.find_element_by_id('submit')
        permalink_save.click
        
        time.sleep(5)

        permalink_save = driver.find_element_by_id('submit')
        permalink_save.click


        # Start better search replace block 
        url = f"http://{i['URL']}/wp-admin/tools.php?page=better-search-replace"
        driver.get(url)
        time.sleep(5)
        search_for = driver.find_element_by_name('search_for')
        search_for.clear()
        search_for.send_keys('shakertemplate.icu')

        replace_with = driver.find_element_by_name('replace_with')
        replace_with.clear()
        replace_with.send_keys(domain)

        el = driver.find_element_by_id('bsr-table-select')
        for option in el.find_elements_by_tag_name('option'):
            if 'post' not in option.text:
                option.click()

        dry_run = driver.find_element_by_id('dry_run')
        dry_run.click()

        time.sleep(1)

        submit = driver.find_element_by_id('bsr-submit')
        submit.click()

        time.sleep(20)

        driver.get(url)
        search_for = driver.find_element_by_name('search_for')
        search_for.clear()
        search_for.send_keys('{site_title}')

        replace_with = driver.find_element_by_name('replace_with')
        replace_with.clear()
        if site_title:
            replace_with.send_keys(site_title)
        else:
            replace_with.send_keys(domain)

        el = driver.find_element_by_id('bsr-table-select')
        for option in el.find_elements_by_tag_name('option'):
            if 'post' not in option.text:
                option.click()

        dry_run = driver.find_element_by_id('dry_run')
        dry_run.click()

        time.sleep(1)

        submit = driver.find_element_by_id('bsr-submit')
        submit.click()
        time.sleep(20)

        driver.get(url)
        search_for = driver.find_element_by_name('search_for')
        search_for.clear()
        search_for.send_keys('offer_link')

        replace_with = driver.find_element_by_name('replace_with')
        replace_with.clear()
        replace_with.send_keys(offer_link)


        el = driver.find_element_by_id('bsr-table-select')
        for option in el.find_elements_by_tag_name('option'):
            if 'post' not in option.text:
                option.click()

        dry_run = driver.find_element_by_id('dry_run')
        dry_run.click()

        time.sleep(1)

        submit = driver.find_element_by_id('bsr-submit')
        submit.click()

        time.sleep(20)

        if spin_the_posts:
            spin_posts = True
        else:
            spin_posts = False
        while spin_posts:
          time.sleep(3)
          driver.get(f'http://{domain}/wp-admin/post.php?post={shaker_page_id}&action=edit')
          time.sleep(3)
          save_draft = driver.find_element_by_id('save-post')
          save_draft.click()
          increment = i['Increment']
          total = i['Total']

          num_posts_to_spin = driver.find_element_by_name('serpshaker[numberOfPosts]')
          cur_value = num_posts_to_spin.get_attribute('value')
          try:
              new_num_posts_to_spin_value = int(cur_value) + increment
          except:
              new_num_posts_to_spin_value = increment
          num_post_to_skip = driver.find_element_by_name('serpshaker[numberOfSkip]')
          num_post_to_skip.clear()
          num_post_to_skip.send_keys(new_num_posts_to_spin_value - increment)
          if new_num_posts_to_spin_value > total:
              print('Break Here')
              spin_posts = False
              new_num_posts_to_spin_value = total
          num_posts_to_spin.clear()
          num_posts_to_spin.send_keys(new_num_posts_to_spin_value)
          num_post_to_skip = driver.find_element_by_name('serpshaker[numberOfDays]')
          num_post_to_skip.clear()
          num_post_to_skip.send_keys(3000)
          backdate = driver.find_element_by_css_selector(
                              "input[type='radio'][value='past']")
          backdate.click()
          category_input = driver.find_element_by_id('ss_category')
          category_input.clear()
          category_input.send_keys(category)
          build_button = driver.find_element_by_id('build')
          build_button.click()
          
        driver.get(f"http://{domain}/wp-admin/profile.php")

        new_pw_button = driver.find_element_by_class_name('wp-generate-pw')
        new_pw_button.click()

        characters = string.ascii_letters + string.punctuation  + string.digits
        gen_password =  "".join(choice(characters) for x in range(randint(12, 16))).replace('/','').replace('\\','')
        tmp_arr.append(gen_password)
        print (tmp_arr)

        new_password = driver.find_element_by_id('pass1')
        new_password.clear()
        new_password.send_keys(gen_password)

        submit = driver.find_element_by_id('submit')
        submit.click()
        obj = {
            "password": gen_password
        }
        # db.collection('serp_site_master_list').document(domain).update(obj)
        pw_list.append_row(tmp_arr)


        new_domain_ss.delete_rows(idx+2)
    except Exception as e:
        print(f"Error Happened ======> {domain}")
        print(f"Error Happened {e}")
        continue
driver.close()