import os
import CloudFlare
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

CF_API_TOKEN = os.getenv(('CF_API_TOKEN'))
EMAIL = os.getenv('EMAIL')

cloud_flare = CloudFlare.CloudFlare(email=EMAIL, token=CF_API_TOKEN)


def add_dns_to_cloudflare(domain, name, content, proxied=False, dns_type='CNAME', ssl_setting='flexible'):
    zone_name = domain.replace('www.', '')
    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    dns_record = {'name': name, 'type': dns_type,
                  'content': content, 'proxied': proxied}
    print(f"Adding {dns_record} to cloudflare")
    try:
        r = cloud_flare.zones.dns_records.post(zone_id, data=dns_record)
    except Exception as e:
        print(e)
        print(zone_id)
        print(dns_record)
    try:
        settings_ssl = cloud_flare.zones.settings.ssl.patch(
            zone_id, data={'value': ssl_setting})
    except:
        print('SSL Not Set')
    return


def add_domain_to_cloudflare(zone_name):

    try:
        zone_info = cloud_flare.zones.post(
            data={'jump_start': False, 'name': zone_name})
        zone_id = zone_info['id']
        r = cloud_flare.zones.settings.always_use_https.patch(
            zone_id, data={'value': 'on'})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print('/zones.post %s - %d %s' % (zone_name, e, e))
    except Exception as e:
        print('/zones.post %s - %s' % (zone_name, e))

    print('Finished zone %s ...' % (zone_name))
    return


def add_page_rule(zone_name):

    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    data = {
        'targets': [{'target': 'url', 'constraint': {'operator': 'matches', 'value': f'*{zone_name}/*.js'}}],
        "actions": [{'id': 'cache_level', 'value': 'bypass'}],
        "priority": 1,
        "status": 'active'
    }
    r = cloud_flare.zones.pagerules.post(zone_id, data=data)
    return


def edit_page_rule(zone_name):
    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    r = cloud_flare.zones.pagerules.get(zone_id)
    print(r)
    rule_id = r[0]['id']
    tid = zone_name.replace('-', '').replace('.', '').replace('/', '')
    print(tid)
    data = {
        'targets': [{'target': 'url', 'constraint': {'operator': 'matches', 'value': f'*{zone_name}/*.js'}}],
        "actions": [{'id': 'cache_level', 'value': 'bypass'}],
        "priority": 1,
        "status": 'active'
    }
    p = cloud_flare.zones.pagerules.patch(zone_id, rule_id, data=data)
    return

def get_page_rule(zone_name):
    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    r = cloud_flare.zones.pagerules.get(zone_id)
    print(r)
    return


def delete_page_rule(zone_name):
    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    try:
        r = cloud_flare.zones.pagerules.get(zone_id)
        print(r)
        rule_id = r[0]['id']
        p = cloud_flare.zones.pagerules.delete(zone_id, rule_id)
    except:
        pass
    return



def delete_dns_records(zone_name):
    zones = cloud_flare.zones.get(params={'name': zone_name, 'per_page': 1})
    zone_id = zones[0]['id']
    dns_records = cloud_flare.zones.dns_records.get(zone_id)
    for record in dns_records:
        record_id = record['id']
        r = cloud_flare.zones.dns_records.delete(zone_id, record_id)


def manually_add_to_cloudflare():
    domains = [
    ]
    a_record = '173.255.193.125'
    for domain in domains:
        delete_dns_records(domain)
        # add_domain_to_cloudflare(domain)
        add_dns_to_cloudflare(domain, '@', a_record,
                              dns_type='A', proxied=True, ssl_setting='flexible')
        add_dns_to_cloudflare(domain, 'www', domain,
                              proxied=True, ssl_setting='flexible')

    return

# get_page_rule('austintexasbusinesslist.com')
# manually_add_to_cloudflare()
