import os
import glob
import pandas as pd
os.chdir("/Users/adam.windsor/sitegrinder/kw_lists_raw_csv")
new_filename = input("Enter New Filename - No extension\n")
extension = 'csv'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]

#combine all files in the list
df = pd.concat([pd.read_csv(f) for f in all_filenames ])

combined_csv = df['Keyword'].str.title()
combined_csv.drop_duplicates(inplace=True)
# new_filename = str(df.iat[0,1])
print(new_filename)
#export to csv
print(combined_csv.shape)
combined_csv.to_csv( f"/Users/adam.windsor/sitegrinder/kw_lists_combined/{new_filename}_combined_keyword_list.csv", header=False, index=False, encoding='utf-8-sig')

for f in all_filenames:
    try:
        print(f)
        os.remove(f)
    except:
        pass
# /Users/adam.windsor/sitegrinder/src/sg-api/combine_csv.py