#!/usr/bin/python

from dotenv import load_dotenv
from cloudflare import add_dns_to_cloudflare
import httplib2
import argparse
from apiclient import errors
from googleapiclient.discovery import build
from oauth2client.client import OAuth2WebServerFlow
from oauth2client import client
from oauth2client import file
from oauth2client import tools
from time import sleep
from google.cloud import firestore
import webbrowser
db_collection = 'serp_site_master_list'
load_dotenv()
db = firestore.Client()
creds = 'gsc_creds.json'   # Credential file from GSC


def authorize_creds(creds):
    # Variable parameter that controls the set of resources that the access token permits.
    SCOPES = ['https://www.googleapis.com/auth/webmasters',
              'https://www.googleapis.com/auth/siteverification']

    # Path to client_secrets.json file
    CLIENT_SECRETS_PATH = creds

    # Create a parser to be able to open browser for Authorization
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        parents=[tools.argparser])
    flags = parser.parse_args([])

    flow = client.flow_from_clientsecrets(
        CLIENT_SECRETS_PATH, scope=SCOPES,
        message=tools.message_if_missing(CLIENT_SECRETS_PATH))

    # Prepare credentials and authorize HTTP
    # If they exist, get them from the storage object
    # credentials will get written back to a file.
    storage = file.Storage('authorizedcreds.dat')
    credentials = storage.get()

    # If authenticated credentials don't exist, open Browser to authenticate
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(flow, storage, flags)
    http = credentials.authorize(http=httplib2.Http())
    site_verification_service = build('siteverification', 'v1', http=http)
    webmasters_service = build('webmasters', 'v3', http=http)
    return site_verification_service, webmasters_service


site_verification_service, webmasters_service = authorize_creds(creds)


def get_gsc_validation_code(domain):
    data = {
        "verificationMethod": "DNS_TXT",
        "site": {
            "identifier": f"{domain}",
            "type": "INET_DOMAIN"
        }
    }

    gsc_validation_code = site_verification_service.webResource().getToken(
        body=data).execute()
    print(gsc_validation_code)
    return gsc_validation_code


def add_site_to_gsc(domain):
    webmasters_service.sites().add(siteUrl=f'sc-domain:{domain}').execute()
    return


def verify_site_with_gsc(domain):
    data = {
        "verificationMethod": "DNS_TXT",
        "site": {
            "identifier": f"{domain}",
            "verificationMethod": "DNS_TXT",
            "type": "INET_DOMAIN"
        }
    }
    site_verification_service.webResource().insert(body=data).execute()
    return


def add_sitemap_to_gsc(domain, sitemap_url):
    webmasters_service.sitemaps().submit(
        siteUrl=f'sc-domain:{domain}', feedpath=f"https://{domain}/{sitemap_url}").execute()
    return


def get_list_from_firestore():
    sites = db.collection(db_collection).get()
    tmp_array = []
    for s in sites:
        tmp_array.append(s.to_dict())
    return tmp_array
# domains = [
# 'villaromawnc.com'
# ]

# for domain in domains:
#     add_site_to_gsc(domain)
#     validation_code = get_gsc_validation_code(domain)
#     add_dns_to_cloudflare(
#         domain, '@', validation_code['token'], dns_type='TXT', ssl_setting='flexible')
#     # verify_site_with_gsc(domain)
#     webbrowser.open(
#         f'https://search.google.com/u/1/search-console?resource_id=sc-domain%3A{domain}')
#     sleep(15)
#     add_sitemap_to_gsc(domain, 'wp-sitemap.xml')


# https://search.google.com/u/1/search-console?resource_id=sc-domain%3Adircyt.com
# Print the URLs of all websites you are verified for.
site_list = get_list_from_firestore()
sites = [site_list['site_url'] for s in site_list]
tmp_arr = []
for s in site_list:
    if s.get('site_url'):
        tmp_arr.append(s['site_url'])
# webmasters_service.sites().add(siteUrl='sc-domain:adamwindsor.com').execute()
# s = webmasters_service.sites().get(siteUrl='sc-domain:adamwindsor.com').execute()
# print(s)
for site_url in tmp_arr:
    # print(site_url)
    # Retrieve list of sitemaps submitted
    try:
        sitemaps = webmasters_service.sitemaps().list(siteUrl=f'sc-domain:{site_url}').execute()
        if 'sitemap' in sitemaps:
            sitemap_urls = [s['path'] for s in sitemaps['sitemap']]
            print("  " + "\n  ".join(sitemap_urls))
    except Exception as e:
        print(f"Couldn't get site map for {site_url}")


# # Retrieve list of properties in account
# b = webmasters_service.sites().list().execute()
# print(b)
