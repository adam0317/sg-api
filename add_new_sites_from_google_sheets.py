from time import sleep
import gspread
from google.cloud import firestore
from dotenv import load_dotenv
import webbrowser
from cloudflare import add_dns_to_cloudflare, add_domain_to_cloudflare, delete_dns_records, add_page_rule
# from search_console import add_site_to_gsc, get_gsc_validation_code, verify_site_with_gsc, add_sitemap_to_gsc

load_dotenv()

db = firestore.Client()
gc = gspread.service_account(
    filename='google_credentials.json')
sh = gc.open('Serp Site List')
new_domain_ss = sh.worksheet('New Domains For Setup')



def add_to_firestore(site_list):

    for i in site_list:

        domain = i['Domain']
        a_record = i['Server IP']
        if db.collection('serp_site_master_list').document(domain).get().exists:
            print(f'Skipping {domain}')
            continue
        if not a_record:
            print(f'skipping {domain} - No IP assigned')
            continue
        obj = {
            "domain_name": f"{domain}",
            "site_url": f"{domain}",
            "tid": f"{domain}",
            "setup_s3_complete": True,
            "build_status": f"Wordpress",
            "date_added": f"{firestore.SERVER_TIMESTAMP}",
            "google_sitemap_submitted": False,
            "bing_sitemap_submitted": False,
            "password": 'wbG6Y0pVUFiE'
        }
        add_domain_to_cloudflare(domain)
        delete_dns_records(domain)

        add_dns_to_cloudflare(domain, '@', a_record,
                              dns_type='A', proxied=True, ssl_setting='flexible')
        add_dns_to_cloudflare(domain, 'www', domain,
                              proxied=True, ssl_setting='flexible')
        add_page_rule(domain)
        try:
            db.collection('serp_site_master_list').document(domain).update(obj)
        except:
            db.collection('serp_site_master_list').document(domain).set(obj)


new_sites = new_domain_ss.get_all_records()

if new_sites:
    add_to_firestore(new_sites)
