from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import re

import time
import datetime
from google.cloud import firestore
from dotenv import load_dotenv
load_dotenv()


chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")
import webbrowser

db_collection = 'serp_site_master_list'
change_over_time_collection = 'indexing_over_time'
db = firestore.Client()

# webbrowser.open('https://docs.google.com/spreadsheets/d/1rdhlgpiFfL-uF-g4JqR3-FSE8j4QlK57IUaUBb4B1xk/edit#gid=80786711')
def initial_index_check(domain, search_engine):
  driver = webdriver.Chrome(options=chrome_options)
  result = False
  if search_engine == 'Google':
    driver.get(f'https://www.google.com/search?q=site:{domain}')
    try:
      result = driver.find_element_by_id('result-stats').get_attribute('innerHTML').split('<nobr>')[0]
      a = result.split(' ')
      if len(a) > 1:
        if  len(a) == 2:
          result = a[0]
        else:
          result = a[1]
    except:
      pass
    
  if search_engine == 'Bing':
    driver.get(f'https://www.bing.com/search?q=site:{domain}')
    try:
      results = driver.find_element_by_id('b_tween').get_attribute('innerHTML')
      number = driver.find_elements_by_class_name('sb_count')
      result = number[0].get_attribute('innerHTML').split(' ')[0]
    except:
      pass

  print(f'Searching {search_engine} for {domain} had {result}')
  return result


data = []
firestore_data = db.collection(db_collection).get()
for i in firestore_data:
    data.append(i.to_dict())

for i in data:
  # if i['build_status'] == 'indexing':
  bing_indexed = initial_index_check(i['domain_name'], 'Bing')
  if bing_indexed:
    bing_num = int(re.sub('[^0-9]', '', bing_indexed))
    try:
      db.collection(db_collection).document(i['domain_name']).update({'bing_index': bing_num, 'bing_index_last_updated': firestore.SERVER_TIMESTAMP})
      db.collection(change_over_time_collection).document().set({'domain_name': i['domain_name'],'bing_index': bing_num, 'created': firestore.SERVER_TIMESTAMP})
    except Exception as e:
      print(f"Couldn't add to collections {e}")
  else:
    bing_num = 0
    try:
      db.collection(db_collection).document(i['domain_name']).update({'bing_index': bing_num, 'bing_index_last_updated': firestore.SERVER_TIMESTAMP})
      db.collection(change_over_time_collection).document().set({'domain_name': i['domain_name'],'bing_index': bing_num, 'created': firestore.SERVER_TIMESTAMP})
    except Exception as e:
      print(f"Couldn't add to collections {e}")
  
  google_indexed = initial_index_check(i['domain_name'], 'Google')
  if google_indexed:
    g_num = int(re.sub('[^0-9]', '', google_indexed))
    try:
      db.collection(db_collection).document(i['domain_name']).update({'google_index': g_num, 'google_index_last_updated': firestore.SERVER_TIMESTAMP})
      db.collection(change_over_time_collection).document().set({'domain_name': i['domain_name'], 'google_index': g_num, 'created': firestore.SERVER_TIMESTAMP})
    except Exception as e:
      print(f"Couldn't add to collections {e}")
  else:
    g_num = 0
    try:
      db.collection(db_collection).document(i['domain_name']).update({'google_index': g_num, 'google_index_last_updated': firestore.SERVER_TIMESTAMP})
      db.collection(change_over_time_collection).document().set({'domain_name': i['domain_name'], 'google_index': g_num, 'created': firestore.SERVER_TIMESTAMP})
    except Exception as e:
      print(f"Couldn't add to collections {e}")
    
  time.sleep(60)
  

