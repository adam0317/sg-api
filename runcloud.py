import requests
import os
from dotenv import load_dotenv, find_dotenv
from requests.models import HTTPBasicAuth
import time
load_dotenv(find_dotenv())

RUNCLOUD_API_KEY = os.getenv('RUNCLOUD_API_KEY')
RUNCLOUD_API_SECRET = os.getenv('RUNCLOUD_API_SECRET')

base_url = 'https://manage.runcloud.io/api/v2'
headers = {'accept': 'application/json', 'content-type': 'application/json'}

r = requests.get(f'{base_url}/ping', auth=HTTPBasicAuth(RUNCLOUD_API_KEY,
                                                        RUNCLOUD_API_SECRET), headers=headers)
print(r.text)


def get_servers():
    r = requests.get(f'{base_url}/servers', auth=HTTPBasicAuth(
        RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers)
    data = r.json()
    print(data['data'])
    return data


def get_webapps(server_id):
    data = requests.get(f'{base_url}/servers/{server_id}/webapps', auth=HTTPBasicAuth(
        RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()

    total_pages = data['meta']['pagination']['total_pages']
    count = 1
    sites = data['data']
    for site in sites:

        id = site['id']
        time.sleep(1)
        domain = requests.get(f"{base_url}/servers/{server_id}/webapps/{id}/domains",
                              auth=HTTPBasicAuth(RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()
        try:
          if len(domain['data']) == 1:
              domain_name = f"www.{domain['data'][0]['name']}"
              www_domain = f'{{"name":"{domain_name}"}}'
              time.sleep(1)
              new_domain = requests.post(f"{base_url}/servers/{server_id}/webapps/{id}/domains", auth=HTTPBasicAuth(
                  RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers, data=www_domain)
        except:
          print('Something went wrong')
    keep_going = True
    while keep_going:
        if count == total_pages:
            keep_going = False
            break
        count += 1
        time.sleep(1)
        data = requests.get(f"{data['meta']['pagination']['links']['next']}", auth=HTTPBasicAuth(
            RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()
        sites = data['data']
        for site in sites:

            id = site['id']
            domain = requests.get(f"{base_url}/servers/{server_id}/webapps/{id}/domains",
                                  auth=HTTPBasicAuth(RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()
                                  
            try:
              if len(domain['data']) == 1:
                  domain_name = f"www.{domain['data'][0]['name']}"
                  www_domain = f'{{"name":"{domain_name}"}}'
                  time.sleep(1)
                  new_domain = requests.post(f"{base_url}/servers/{server_id}/webapps/{id}/domains", auth=HTTPBasicAuth(
                      RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers, data=www_domain)
                  print(new_domain.text)
            except Exception as e:
              print('something went wrong')
              print(e)

# servers = get_servers()
# for server in servers['data']:
#   print(f"starting server id {server['id']}")
#   get_webapps(server['id'])



def get_clone_list():
    servers = ['linode 1', 'linode 2', 'linode 3']

    server_id = '129820'
    final_dataset = []
    sites_to_clone = []

    r = requests.get(f'{base_url}/servers/{server_id}/webapps',
                     auth=HTTPBasicAuth(RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers)
    data = r.json()
    total_pages = data['meta']['pagination']['total_pages']
    # print(data)
    sites = data['data']
    for site in sites:
        tmp_obj = {}
        id = site['id']
        domain = requests.get(f"{base_url}/servers/{server_id}/webapps/{id}/domains",
                              auth=HTTPBasicAuth(RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()
        domain_name = domain['data'][0]['name']
        print(domain_name)
        tmp_obj['domain_name'] = domain_name
        tmp_obj['id'] = id
        sites_to_clone.append(tmp_obj)

    count = 1
    keep_going = True
    while keep_going:
        if count == total_pages:
            keep_going = False
            continue
        count += 1
        r = requests.get(f"{data['meta']['pagination']['links']['next']}", auth=HTTPBasicAuth(
            RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers)
        data = r.json()
        sites = data['data']

        time.sleep(1)
        for site in sites:
            time.sleep(1)
            try:
                tmp_obj = {}
                id = site['id']
                domain = requests.get(f"{base_url}/servers/{server_id}/webapps/{id}/domains", auth=HTTPBasicAuth(
                    RUNCLOUD_API_KEY, RUNCLOUD_API_SECRET), headers=headers).json()
                domain_name = domain['data'][0]['name']
                print(domain_name)
                tmp_obj['domain_name'] = domain_name
                tmp_obj['id'] = id
                sites_to_clone.append(tmp_obj)
            except:
                print(f"No domain name found for {site}")
                continue

    server_count = 1
    for server in servers:
        if server_count % 50 == 0:
            continue
        server_count += 1
        for site in sites_to_clone:
            site['server'] = server

    print(sites_to_clone)
    print(len(sites_to_clone))

    import json
    import csv

    with open('sites.json') as json_file:
        data = json.load(json_file)

    with open("data.csv", "w") as file:
        csv_file = csv.writer(file)
        for item in data:
            csv_file.writerow(
                [item['domain_name'], item['server'], item['id']])
